const heading = document.querySelector('.heading');
const textHeading = heading.innerText;
const arrText = [...textHeading];

const textContent = (param) => {
    const textSpan = param.map(item => `<span>${item}</span>`);
    heading.innerHTML = textSpan.join('');
};
textContent(arrText);
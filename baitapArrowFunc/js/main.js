const colorList = [
    "pallet", 
    "viridian", 
    "pewter", 
    "cerulean", 
    "vermillion", 
    "lavender", 
    "celadon", 
    "saffron", 
    "fuschia", 
    "cinnabar"
];

// Render list button color
const listColor = () => {
    let buttonList = '';

    colorList.map((color, index) => {
        if(index == 0) {
            buttonList += `
                <button class="color-button ${color} active" onclick="changeColor('${color}') "></button>
            `;
        } else {
            buttonList += `
                <button class="color-button ${color}" onclick="changeColor('${color}')"></button>
            `;
        };
    });
    document.getElementById('colorContainer').innerHTML = buttonList;
};
listColor();

// Change color to house
const changeColor = (color) => {
    document.getElementById('house').className = `house ${color}`;

    document.querySelectorAll('.color-button').forEach(item => { 
        item.classList.remove('active'); 
        event.target.classList.add('active'); 
    });
};
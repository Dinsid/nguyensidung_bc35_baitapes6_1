const dtb = (...param) => {
    let sum = 0;
    param.forEach(item => sum += item);
    return sum;
};

const tinhLop1 = () => {
    let maths = document.getElementById('inpToan').value * 1;
    let physics = document.getElementById('inpLy').value * 1;
    let chemistry = document.getElementById('inpHoa').value * 1;

    const classOne = dtb(maths, physics, chemistry) / 3;
    document.getElementById('tbKhoi1').innerHTML = classOne.toFixed(2);
};

const tinhLop2 = () => {
    let literature = document.getElementById('inpVan').value * 1;
    let historyNum = document.getElementById('inpSu').value * 1;
    let geography = document.getElementById('inpDia').value * 1;
    let english = document.getElementById('inpEnglish').value * 1;

    const classTwo = dtb(literature, historyNum, geography, english) / 4;
    document.getElementById('tbKhoi2').innerHTML = classTwo.toFixed(2);
};